﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameDevTV.Utils
{
    [System.Serializable]
    public class Condition
    {
        [SerializeField]
        Disjunction[] and;

        public bool Check(IEnumerable<IPredicateEvaluator> evaluators)
        {
            foreach (Disjunction dis in and)
            {
                if (!dis.Check(evaluators))
                {
                    return false;
                }
            }
            return true;
        }

        public Condition Clone()
        {
            Condition newCondition = new Condition();
            newCondition.and = new Disjunction[and.Length];
            for (int i = 0; i < and.Length; i++)
            {
                newCondition.and[i] = and[i].Clone();
            }
            return newCondition;
        }

        [System.Serializable]
        class Disjunction
        {
            [SerializeField]
            Predicate[] or;

            public bool Check(IEnumerable<IPredicateEvaluator> evaluators)
            {
                foreach (Predicate pred in or)
                {
                    if (pred.Check(evaluators))
                    {
                        return true;
                    }
                }
                return false;
            }

            public Disjunction Clone()
            {
                Disjunction newDisjunction = new Disjunction();
                newDisjunction.or = new Predicate[or.Length];
                for (int i = 0; i < or.Length; i++)
                {
                    newDisjunction.or[i] = or[i].Clone();
                }
                return newDisjunction;
            }
        }

        [System.Serializable]
        class Predicate
        {
            [SerializeField]
            string predicate;
            [SerializeField]
            string[] parameters;
            [SerializeField]
            bool negate = false;

            public Predicate Clone()
            {
                Predicate newPredicate = new Predicate();
                newPredicate.predicate = predicate;
                newPredicate.parameters = parameters;
                newPredicate.negate = negate;
                return newPredicate;
            }
            
            public bool Check(IEnumerable<IPredicateEvaluator> evaluators)
            {
                foreach (var evaluator in evaluators)
                {
                    bool? result = evaluator.Evaluate(predicate, parameters);
                    if (result == null)
                    {
                        continue;
                    }

                    if (result == negate) return false;
                }
                return true;
            }
        }
    }
}