﻿using RPG.InputReading;
using UnityEngine;

namespace RPG.UI.Inventories
{
    public class InventoryWindow : WindowController
    {
        protected override void Subscribe()
        {
            GameObject player = GameObject.FindWithTag("Player");
            player.GetComponent<InputReader>().InventoryEvent += ToggleWindow;
            gameObject.SetActive(false);
        }

        protected override void Unsubscribe()
        {
            GameObject player = GameObject.FindWithTag("Player");
            if (player.CompareTag("Player"))
            {
                
            }
            player.GetComponent<InputReader>().InventoryEvent -= ToggleWindow;
        }
    }
}