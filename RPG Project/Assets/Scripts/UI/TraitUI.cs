using RPG.InputReading;
using RPG.Stats;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.UI
{
    public class TraitUI : WindowController
    {
        [SerializeField] TextMeshProUGUI unassignedPointsText;
        [SerializeField] Button commitButton;

        TraitStore playerTraitStore = null;

        private void Start()
        {
            playerTraitStore = GameObject.FindGameObjectWithTag("Player").GetComponent<TraitStore>();
            commitButton.onClick.AddListener(playerTraitStore.Commit);
            CloseWindow();
        }

        private void Update() {
            unassignedPointsText.text = playerTraitStore.GetUnassignedPoints().ToString();
        }

        protected override void Subscribe()
        {
            GameObject.FindWithTag("Player").GetComponent<InputReader>().TraitStoreEvent += ToggleWindow;
        }

        protected override void Unsubscribe()
        {
            GameObject.FindWithTag("Player").GetComponent<InputReader>().TraitStoreEvent -= ToggleWindow;
        }
    }
}