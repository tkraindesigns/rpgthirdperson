using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class WindMillTurn : MonoBehaviour
{
    [FormerlySerializedAs("rotationSpeed")] [SerializeField] private Vector3 rotationAxis = Vector3.forward;
    [SerializeField] private float speed = 5f;

    private float currentSpeed;

    void Start()
    {
        currentSpeed = speed;
    }
    
    // Update is called once per frame
    void Update()
    {
        Vector3 eulers = transform.localEulerAngles;
        eulers += rotationAxis * (Time.deltaTime * currentSpeed);
        transform.localEulerAngles = eulers;
        currentSpeed += Random.Range(0, Mathf.Sin(Time.time));
        currentSpeed = Mathf.Clamp(currentSpeed, speed *.9f, speed * 1.2f);
    }
}
