using System;
using RPG.UI;
using UnityEngine;
using UnityEngine.InputSystem;

namespace RPG.InputReading
{
    public class InputReader : MonoBehaviour, Controls.IPlayerActions, Controls.IUIActions
    {
        public bool IsAttacking { get; private set; }
        public bool IsBlocking { get; private set; }
        public Vector2 MovementValue { get; private set; }

        public event Action JumpEvent;
        public event Action DodgeEvent;
        public event Action TargetEvent;

        public event Action CancelEvent;
        public event Action InventoryEvent;
        public event Action TraitStoreEvent;
        
        public event Action AttackDownEvent;

        public event Action PickupEvent;
        public event Action DialogueEvent;
        public event Action ShopEvent;
        

        private Controls controls;

        private void Start()
        {
            controls = new Controls();
            controls.Player.SetCallbacks(this);
            controls.UI.SetCallbacks(this);
            controls.Player.Enable();
            controls.UI.Enable();
            WindowController.OnAnyWindowOpened += DisableControls;
            WindowController.OnAllWindowsClosed += EnableControls;
        }

        void EnableControls()
        {
            controls.Player.Enable();
        }

        void DisableControls()
        {
            controls.Player.Disable();
        }
        
        
        private void OnDestroy()
        {
            WindowController.OnAnyWindowOpened -= DisableControls;
            WindowController.OnAllWindowsClosed -= EnableControls;
            controls.Player.Disable();
            controls.UI.Disable();
        }

        public void OnJump(InputAction.CallbackContext context)
        {
            if (!context.performed) { return; }

            JumpEvent?.Invoke();
        }

        public void OnDodge(InputAction.CallbackContext context)
        {
            if (!context.performed) { return; }

            DodgeEvent?.Invoke();
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            MovementValue = context.ReadValue<Vector2>();
        }

        public void OnLook(InputAction.CallbackContext context)
        {

        }

        public void OnTarget(InputAction.CallbackContext context)
        {
            if (!context.performed) { return; }

            TargetEvent?.Invoke();
        }

        public void OnAttack(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                IsAttacking = true;
                AttackDownEvent?.Invoke();
            }
            else if (context.canceled)
            {
                IsAttacking = false;
            }
        }

        public void OnBlock(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                IsBlocking = true;
            }
            else if (context.canceled)
            {
                IsBlocking = false;
            }
        }

        public void OnInventory(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                InventoryEvent?.Invoke();
            }
        }

        public void OnCancel(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                CancelEvent?.Invoke();
            }
        }

        public void OnTrait(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                TraitStoreEvent?.Invoke();
            }
        }

        public void OnPickup(InputAction.CallbackContext context)
        {
            if(context.ReadValueAsButton())
            {
                PickupEvent?.Invoke();
            }
        }

        public void OnDialogue(InputAction.CallbackContext context)
        {
            if (context.ReadValueAsButton())
            {
                DialogueEvent?.Invoke();
            }
        }

        public void OnShop(InputAction.CallbackContext context)
        {
            if (context.ReadValueAsButton())
            {
                ShopEvent?.Invoke();
            }
        }
    }
}