﻿using UnityEngine;

namespace RPG.States
{
    public abstract class StateMachine : MonoBehaviour
    {
        private State currentState;

        public void SwitchState(State newState)
        {
            currentState?.Exit();
            currentState = newState;
            currentState?.Enter();
        }
        
        private void Update()
        {
            currentState?.Tick(Time.deltaTime); 
        }
    }
}