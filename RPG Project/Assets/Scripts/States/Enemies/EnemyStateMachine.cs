﻿using System;
using RPG.Abilities;
using RPG.Attributes;
using RPG.Combat;
using RPG.Control;
using RPG.Core;
using RPG.Movement;
using RPG.Stats;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace RPG.States.Enemies
{
    public class EnemyStateMachine : StateMachine, ITargetProvider
    {
        [field: SerializeField] public Animator Animator { get; private set; }
        [field: SerializeField] public CharacterController CharacterController { get; private set; }
        [field: SerializeField] public ForceReceiver ForceReceiver { get; private set; }
        [field: SerializeField] public NavMeshAgent Agent { get; private set; }
        [field: SerializeField] public PatrolPath PatrolPath { get; private set; }
        [field: SerializeField] public Fighter Fighter { get; private set; }
        [field: SerializeField] public BaseStats BaseStats { get; private set; }
        [field: SerializeField] public Health Health { get; private set; }
        [field: SerializeField] public CooldownTokenManager CooldownTokenManager { get; private set; }

        [field: SerializeField] public float PlayerChasingRange { get; private set; } = 10.0f;
        [field: SerializeField] public float MovementSpeed { get; private set; } = 4.0f;
        [field: SerializeField] public float RotationSpeed { get; private set; } = 45f;
        [field: SerializeField] public float CrossFadeDuration { get; private set; } = .1f;
        [field: SerializeField] public float AnimatorDampTime { get; private set; } = .1f;
        [SerializeField] private float aggroTime = 3f;
        
        public float PlayerChasingRangedSquared { get; private set; }
        public GameObject Player { get; private set; }

        public Blackboard Blackboard = new Blackboard();
        
        private void OnValidate()
        {
            if (!Animator) Animator = GetComponentInChildren<Animator>();
            if (!CharacterController) CharacterController = GetComponent<CharacterController>();
            if (!ForceReceiver) ForceReceiver = GetComponent<ForceReceiver>();
            if (!Agent) Agent = GetComponent<NavMeshAgent>();
            if (!Fighter) Fighter = GetComponent<Fighter>();
            if (!BaseStats) BaseStats = GetComponent<BaseStats>();
            if (!Health) Health = GetComponent<Health>();
            if (!CooldownTokenManager) CooldownTokenManager = GetComponent<CooldownTokenManager>();
        }

        private void Awake()
        {
            if (Fighter)
            {
                Fighter.OnWeaponChanged += Handle_WeaponChanged;
            }
        }

        private void Handle_WeaponChanged(WeaponConfig weaponConfig)
        {
            if (weaponConfig)
            {
                PlayerChasingRange = weaponConfig.GetTargetingRange() * .8f;
                PlayerChasingRange = Mathf.Max(PlayerChasingRange, weaponConfig.GetRange()); //Never less than Attack Range
                PlayerChasingRangedSquared = PlayerChasingRange * PlayerChasingRange;
            }
        }

        private void Start()
        {
            Agent.updatePosition = false;
            Agent.updateRotation = false;
            PlayerChasingRangedSquared = PlayerChasingRange * PlayerChasingRange;
            Player = GameObject.FindGameObjectWithTag("Player");
            Blackboard["Level"] = BaseStats.GetLevel();
            if (Health.IsDead())
            {
                SwitchState(new EnemyDeathState(this));
            }
            else
            {
                SwitchState(new EnemyIdleState(this));
            }
            Health.onDie.AddListener(() =>
            {
                SwitchState(new EnemyDeathState(this));
            });
            Health.onResurrection.AddListener(() =>
            {
                SwitchState(new EnemyIdleState(this));
            });
            Health.OnDamageTaken += () =>
            {
                CooldownTokenManager.SetCooldown("Aggro", aggroTime, true);
            };
            ForceReceiver.OnForceApplied += Handle_ForceApplied;
        }
        
        private void Handle_ForceApplied(Vector3 force)
        {
            if (Health.IsDead()) return;
            float forceAmount = Random.Range(0, force.sqrMagnitude);
            if (forceAmount > Random.Range(0f, BaseStats.GetLevel()))
            {
                SwitchState(new EnemyImpactState(this));
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, PlayerChasingRange);
        }

        public GameObject GetTarget()
        {
            return Player;
        }
    }
}