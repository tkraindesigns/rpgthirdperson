﻿using UnityEngine;

namespace RPG.States.Enemies
{
    public class EnemyDeathState : EnemyBaseState
    {
        private static readonly int DieHash = Animator.StringToHash("Die");
        
        public EnemyDeathState(EnemyStateMachine stateMachine) : base(stateMachine)
        {
        }

        public override void Enter()
        {
            stateMachine.Animator.CrossFadeInFixedTime(DieHash, stateMachine.AnimatorDampTime);
        }

        public override void Tick(float deltaTime)
        {
            Move(deltaTime);
        }

        public override void Exit()
        {
            
        }
    }
}