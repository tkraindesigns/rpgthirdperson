﻿using RPG.Attributes;
using UnityEngine;
using UnityEngine.AI;

namespace RPG.States.Enemies
{
    public abstract class EnemyBaseState : State
    {
        protected static readonly int FreeLookBlendTreeHash = Animator.StringToHash("FreeLookBlendTree");
        protected static readonly int FreeLookSpeedHash = Animator.StringToHash("FreeLookSpeed");
        protected static readonly int IdleHash = Animator.StringToHash("Idle");
        
        public EnemyBaseState(EnemyStateMachine stateMachine)
        {
            this.stateMachine = stateMachine;
        }

        protected EnemyStateMachine stateMachine;

        protected bool IsInChaseRange()
        {
            if (stateMachine.Player.GetComponent<Health>().IsDead()) return false;
            return Vector3.SqrMagnitude(stateMachine.transform.position - stateMachine.Player.transform.position) <= stateMachine.PlayerChasingRangedSquared;
        }

        protected bool IsAggrevated() => stateMachine.CooldownTokenManager.HasCooldown("Aggro");
        
        protected void Move(float deltaTime)
        {
            Move(Vector3.zero, deltaTime);
        }
        
        protected void Move(Vector3 direction, float deltaTime)
        {
            Vector3 intendedMovement = (direction + stateMachine.ForceReceiver.Movement) * deltaTime;
            if(SampleNavMesh(stateMachine.transform.position+intendedMovement))
            {
                stateMachine.CharacterController.Move(intendedMovement);
            }
            else
            {
                stateMachine.CharacterController.Move(new Vector3(0, stateMachine.ForceReceiver.Movement.y, 0));
            }
        }
        
        protected bool SampleNavMesh(Vector3 position)
        {
            if (!stateMachine.CharacterController.isGrounded)
            {
                RaycastHit hit;
                bool hasHit = Physics.Raycast(position, Vector3.down, out hit, 2, 1<<LayerMask.NameToLayer("Terrain"));
                if (hasHit) position = hit.point; 
            }
            NavMeshHit navMeshHit;
            bool hasCastToNavMesh = NavMesh.SamplePosition(
                position, out navMeshHit, 1f, NavMesh.AllAreas);
            if (!hasCastToNavMesh) return false;
            return true;
        }
        
        protected void FaceTarget(Vector3 target, float deltaTime)
        {
            Vector3 directionToTarget = target - stateMachine.transform.position;
            directionToTarget.y = 0;
            stateMachine.transform.rotation = Quaternion.Slerp(stateMachine.transform.rotation, Quaternion.LookRotation(directionToTarget), stateMachine.RotationSpeed * deltaTime);
        }
        
        protected float GetNormalizedTime(string tag = "Attack")
        {
            var currentInfo= stateMachine.Animator.GetCurrentAnimatorStateInfo(0);
            var nextInfo = stateMachine.Animator.GetNextAnimatorStateInfo(0);
            if (stateMachine.Animator.IsInTransition(0) && nextInfo.IsTag(tag))
            {
                return nextInfo.normalizedTime;
            }
            else if(!stateMachine.Animator.IsInTransition(0) && currentInfo.IsTag(tag))
            {
                return currentInfo.normalizedTime;
            }

            return 0;
        }
    }
}