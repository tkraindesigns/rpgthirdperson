﻿namespace RPG.States.Enemies
{
    public class EnemyIdleState : EnemyBaseState
    {
        public EnemyIdleState(EnemyStateMachine stateMachine) : base(stateMachine)
        {
        }
        
        private bool idleSpeedReached;

        public override void Enter()
        {
            if (stateMachine.PatrolPath) //If we have path, switch
            {
                stateMachine.SwitchState(new EnemyPatrollingState(stateMachine));
                return;
            }
            stateMachine.Animator.CrossFadeInFixedTime(FreeLookBlendTreeHash, stateMachine.CrossFadeDuration);
        }

        public override void Tick(float deltaTime)
        {
            Move(deltaTime);
            if (IsInChaseRange() || IsAggrevated())
            {
                stateMachine.SwitchState(new EnemyChasingState(stateMachine));
                return;
            }
            if(!idleSpeedReached)
            {
                stateMachine.Animator.SetFloat(FreeLookSpeedHash, 0f, stateMachine.AnimatorDampTime, deltaTime);
                if (stateMachine.Animator.GetFloat(FreeLookSpeedHash) < .05f)
                {
                    stateMachine.Animator.CrossFadeInFixedTime(IdleHash, stateMachine.CrossFadeDuration);
                    idleSpeedReached = true;
                }
            }
        }

        public override void Exit()
        {
            
        }
    }
}