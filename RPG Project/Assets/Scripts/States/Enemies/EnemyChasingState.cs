﻿using UnityEngine;

namespace RPG.States.Enemies
{
    public class EnemyChasingState: EnemyBaseState
    {
        public EnemyChasingState(EnemyStateMachine stateMachine) : base(stateMachine)
        {
        }

        public readonly int TargetingBlendTreeHash = Animator.StringToHash("TargetingBlendTree");
        
        private float attackingRange;
        
        public override void Enter()
        {
            stateMachine.Animator.CrossFadeInFixedTime(TargetingBlendTreeHash, stateMachine.CrossFadeDuration);
            attackingRange = stateMachine.Fighter.GetAttackingRange();
            attackingRange *= attackingRange;
        }

        public override void Tick(float deltaTime)
        {
            if (!IsInChaseRange() && !IsAggrevated())
            {
                stateMachine.SwitchState(new EnemyIdleState(stateMachine));
                return;
            }
            Vector3 lastPosition = stateMachine.transform.position;
            if (IsInAttackingRange())
            {
                if (!stateMachine.CooldownTokenManager.HasCooldown("Attack"))
                {
                    stateMachine.SwitchState(new EnemyAttackingState(stateMachine));
                    return;
                }
                else
                {
                    Move(deltaTime);
                    stateMachine.Animator.SetFloat(FreeLookSpeedHash, 0);
                    return;
                }
            }
            else
            {
                MoveToPlayer(deltaTime);
            }
            Vector3 deltaMovement = lastPosition - stateMachine.transform.position;
            float deltaMagnitude = deltaMovement.magnitude;
            
            if (deltaMagnitude > 0)
            {
                FaceTarget(stateMachine.transform.position - deltaMovement, deltaTime);
                float grossSpeed = deltaMagnitude / deltaTime;
                stateMachine.Animator.SetFloat(FreeLookSpeedHash, grossSpeed/stateMachine.MovementSpeed, stateMachine.AnimatorDampTime, deltaTime);
            }
            else
            {
                FaceTarget(stateMachine.Player.transform.position, deltaTime);
                stateMachine.Animator.SetFloat(FreeLookSpeedHash, 0f);
            }
        }

        private bool IsInAttackingRange()
        {
            return Vector3.SqrMagnitude(stateMachine.Player.transform.position - stateMachine.transform.position) <=
                   attackingRange;
        }

        public override void Exit()
        {
            stateMachine.Agent.ResetPath();
            stateMachine.Agent.velocity = Vector3.zero;
        }

        private void MoveToPlayer(float deltaTime)
        {
            if (stateMachine.Agent.enabled)
            {
                stateMachine.Agent.destination = stateMachine.Player.transform.position;
                Vector3 desiredVelocity = stateMachine.Agent.desiredVelocity.normalized;
                Move(desiredVelocity * stateMachine.MovementSpeed, deltaTime);
                stateMachine.Agent.velocity = stateMachine.CharacterController.velocity;
                stateMachine.Agent.nextPosition = stateMachine.transform.position;
            }
            else
            {
                Move(deltaTime);
            }
        }
    }
}