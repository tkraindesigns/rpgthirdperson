﻿using UnityEngine;

namespace RPG.States.Enemies
{
    public class EnemyImpactState : EnemyBaseState
    {
        private readonly int ImpactHash = Animator.StringToHash("Impact");
        public EnemyImpactState(EnemyStateMachine stateMachine) : base(stateMachine)
        {
        }

        public override void Enter()
        {
            stateMachine.Animator.CrossFadeInFixedTime(ImpactHash, stateMachine.CrossFadeDuration);
            stateMachine.ForceReceiver.OnForceCompleted += Handle_OnForceCompleted;
        }

        public override void Tick(float deltaTime)
        {
            Move(deltaTime);
        }

        public override void Exit()
        {
            stateMachine.ForceReceiver.OnForceCompleted -= Handle_OnForceCompleted;
        }

        private void Handle_OnForceCompleted()
        {
            stateMachine.SwitchState(new EnemyIdleState(stateMachine));
        }
    }
}