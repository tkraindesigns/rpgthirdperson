﻿using RPG.Combat;
using UnityEngine;

namespace RPG.States.Enemies
{
    public class EnemyAttackingState : EnemyBaseState
    {
        private Attack attack;
        private int level;
        private bool triedCombo;
        
        public EnemyAttackingState(EnemyStateMachine stateMachine) : base(stateMachine)
        {
            attack = stateMachine.Fighter.GetCurrentAttack(0);
        }

        public EnemyAttackingState(EnemyStateMachine stateMachine, Attack attack) : base(stateMachine)
        {
            this.attack = attack; 
            stateMachine.Fighter.SetCurrentAttack(attack);
        }
        
        public override void Enter()
        {
            if (!attack)
            {
                stateMachine.SwitchState(new EnemyIdleState(stateMachine));
                return;
            }
            level = stateMachine.Blackboard.GetValueAsInt("Level", 1);
            stateMachine.Animator.CrossFadeInFixedTime(attack.AnimationName, stateMachine.CrossFadeDuration);
        }

        public override void Tick(float deltaTime)
        {
            float normalizedTime = GetNormalizedTime();
            if (attack.NextComboAttack && !triedCombo && normalizedTime > attack.ComboAttackTime)
            {
                if (level*5 > Random.Range(0, 100))
                {
                    stateMachine.SwitchState(new EnemyAttackingState(stateMachine, attack.NextComboAttack));
                    return;
                }
                triedCombo = true;
            }

            if (normalizedTime > .98f)
            {
                stateMachine.SwitchState(new EnemyIdleState(stateMachine));
            }
            FaceTarget(stateMachine.Player.transform.position, deltaTime);
            Move(deltaTime);
        }

        public override void Exit()
        {
            stateMachine.CooldownTokenManager.SetCooldown("Attack", attack.Cooldown);
        }
    }
}