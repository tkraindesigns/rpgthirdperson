﻿using RPG.Combat;
using UnityEngine;

namespace RPG.States.Animations
{
    public class WeaponTypeSetterAnimationState : StateMachineBehaviour
    {
        private Fighter fighter;
        private int weaponType;
        private bool changed;
        private static readonly int WeaponTypeHash = Animator.StringToHash("WeaponType");


        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            Debug.Log($"OnStateEnter {animator}");
            fighter = animator.GetComponent<Fighter>();
            if (fighter == null) fighter = animator.GetComponentInParent<Fighter>();
            if (fighter == null)
            {
                Debug.Log($"No Fighter Found on {animator.gameObject}");
                return;
            }

            fighter.OnWeaponChanged += SetWeaponIndex;
            weaponType = fighter.GetCurrentWeaponType();
            changed = true;
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            if (fighter) fighter.OnWeaponChanged -= SetWeaponIndex;
        }

        private void SetWeaponIndex(WeaponConfig config)
        {
            if (config)
            {
                weaponType = config.AnimatorWeaponType;
                changed = true;
                Debug.Log($"WeaponType = {weaponType}");
            }
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            if (changed)
            {
                Debug.Log($"Updating WeaponType to {weaponType}");
                animator.SetFloat(WeaponTypeHash, weaponType);
                changed = false;
            }
        }
        
    }
}