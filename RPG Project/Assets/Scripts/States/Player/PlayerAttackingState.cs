﻿using RPG.Combat;
using UnityEngine;

namespace RPG.States.Player
{
    public class PlayerAttackingState : PlayerBaseState
    {
        private Attack currentAttack;
        private bool hasCombo;
        public PlayerAttackingState(PlayerStateMachine stateMachine, Attack attack) : base(stateMachine)
        {
            currentAttack = attack;
            
            stateMachine.Fighter.SetCurrentAttack(attack); //Inform fighter of new attack
            
        }

        public PlayerAttackingState(PlayerStateMachine stateMachine, int attack) : base(stateMachine)
        {
            currentAttack = stateMachine.Fighter.GetCurrentAttack(attack); 
        }
        public override void Enter()
        {
            
            if (currentAttack == null)
            {
                SetLocomotionState();
                return;
            }

            if(currentAttack.ApplyRootMotion) stateMachine.Animator.applyRootMotion = true;
            hasCombo = currentAttack.NextComboAttack != null;
            Debug.Log(
                $"Combo {currentAttack.name}, animation name ={currentAttack.AnimationName}, NextCombo {currentAttack.NextComboAttack}, hasCombo {hasCombo}, ComboAttackTime {currentAttack.ComboAttackTime}");
            stateMachine.Animator.CrossFadeInFixedTime(currentAttack.AnimationName, currentAttack.TransitionDuration);
            
        }

        public override void Tick(float deltaTime)
        {
            float normalizedTime = GetNormalizedTime();
            if (hasCombo && normalizedTime>currentAttack.ComboAttackTime && stateMachine.InputReader.IsAttacking)
            {
                stateMachine.SwitchState(new PlayerAttackingState(stateMachine, currentAttack.NextComboAttack));
                return;
            }
            if (normalizedTime > .99f)
            {
                SetLocomotionState();   
            }

            var target = stateMachine.Targeter.CurrentTarget;
            if (target)
            {
                FaceTarget(target.transform.position, deltaTime);
            }
            Move(deltaTime);
        }

        public override void Exit()
        {
            stateMachine.Animator.applyRootMotion = false;
        }
    }
}