﻿using UnityEngine;

namespace RPG.States.Player
{
    public class PlayerImpactState : PlayerBaseState
    {
        private readonly int ImpactHash = Animator.StringToHash("Impact");
        
        public PlayerImpactState(PlayerStateMachine stateMachine) : base(stateMachine)
        {
        }

        public override void Enter()
        {
            stateMachine.Animator.CrossFadeInFixedTime(ImpactHash, stateMachine.CrossFadeDuration);
            stateMachine.ForceReceiver.OnForceCompleted += Handle_OnForceCompleted;
            //Set an absurd cooldown to last through the entire Impact State
            stateMachine.CooldownTokenManager.SetCooldown("Impact", 100f);
        }

        public override void Tick(float deltaTime)
        {
            Move(deltaTime);
        }

        public override void Exit()
        {
            //Set a real cooldown time for the end of the impact.
            stateMachine.CooldownTokenManager.SetCooldown("Impact", stateMachine.ImpactCooldown);
            stateMachine.ForceReceiver.OnForceCompleted -= Handle_OnForceCompleted;
        }

        private void Handle_OnForceCompleted()
        {
            SetLocomotionState();
        }
    }
}