﻿using UnityEngine;

namespace RPG.States.Player
{
    public class PlayerDeathState : PlayerBaseState
    {
        private static readonly int DieHash = Animator.StringToHash("Die");
        
        public PlayerDeathState(PlayerStateMachine stateMachine) : base(stateMachine)
        {
        }

        public override void Enter()
        {
            stateMachine.Animator.CrossFadeInFixedTime(DieHash, AnimatorDampTime);
        }

        public override void Tick(float deltaTime)
        {
            Move(deltaTime);
        }

        public override void Exit()
        {
            
        }
    }
}