﻿using System;
using RPG.Attributes;
using RPG.Combat;
using RPG.Combat.Targeting;
using RPG.Control;
using RPG.Core;
using RPG.Dialogue;
using RPG.InputReading;
using RPG.Inventories;
using RPG.Movement;
using RPG.Shops;
using RPG.Stats;
using UnityEngine;
using Random = UnityEngine.Random;

namespace RPG.States.Player
{
    public class PlayerStateMachine : StateMachine, ITargetProvider
    {
        [field: SerializeField] public InputReader InputReader { get; private set; }
        [field: SerializeField] public CharacterController CharacterController { get; private set; }
        [field: SerializeField] public Animator Animator { get; private set; }
        [field: SerializeField] public AnimationEventRelay AnimationEventRelay { get; private set; }
        [field: SerializeField] public ForceReceiver ForceReceiver { get; private set; }
        [field: SerializeField] public Targeter Targeter { get; private set; }
        [field: SerializeField] public Fighter Fighter { get; private set; }
        [field: SerializeField] public PickupFinder PickupFinder { get; private set; }
        [field: SerializeField] public ConversantFinder ConversantFinder { get; private set; }
        [field: SerializeField] public PlayerConversant PlayerConversant { get; private set; }
        [field: SerializeField] public ShopFinder ShopFinder { get; private set; }
        [field: SerializeField] public Shopper Shopper { get; private set; }
        [field: SerializeField] public Health Health { get; private set; }
        [field: SerializeField] public BaseStats BaseStats { get; private set; }

        [field: SerializeField] public CooldownTokenManager CooldownTokenManager { get; private set; }
       

        [field: SerializeField] public float FreeLookMovementSpeed { get; private set; } = 6.0f;
        [field: SerializeField] public float FreeLookRotationSpeed { get; private set; } = 15f;
        [field: SerializeField] public float TargetingMovementSpeed { get; private set; } = 5.0f;
        [field: SerializeField] public float CrossFadeDuration { get; private set; } = .15f;
        [field: SerializeField] public float ImpactCooldown { get; private set; } = 2.0f;
        
        
        public Transform MainCameraTransform { get; private set; }
        void Start()
        {
            MainCameraTransform = Camera.main.transform;
            if (Health.IsDead())
            {
                SwitchState(new PlayerDeathState(this));
            }
            else
            {
                SwitchState(new PlayerFreeLookState(this));
            }
            Health.onDie.AddListener(() =>
            {
                SwitchState(new PlayerDeathState(this));
            });
            Health.onResurrection.AddListener(() =>
            {
                SwitchState(new PlayerFreeLookState(this));
            });
            ForceReceiver.OnForceApplied += Handle_ForceApplied;
        }

        private void Handle_ForceApplied(Vector3 force)
        {
            if (Health.IsDead()) return;
            if (CooldownTokenManager.HasCooldown("Impact")) return;
            float forceAmount = force.magnitude;
            if (forceAmount > Random.Range(0f, BaseStats.GetLevel()))
            {
                SwitchState(new PlayerImpactState(this));
            }
        }

        private void OnValidate()
        {
            if (InputReader == null) InputReader = GetComponent<InputReader>();
            if (CharacterController == null) CharacterController = GetComponent<CharacterController>();
            if (Animator == null) Animator = GetComponentInChildren<Animator>();
            if (AnimationEventRelay == null) AnimationEventRelay = GetComponentInChildren<AnimationEventRelay>();
            if (ForceReceiver == null) ForceReceiver = GetComponent<ForceReceiver>();
            if (Targeter == null) Targeter = GetComponentInChildren<Targeter>();
            if (Fighter == null) Fighter = GetComponent<Fighter>();
            if (PickupFinder == null) PickupFinder = GetComponentInChildren<PickupFinder>();
            if (ConversantFinder == null) ConversantFinder = GetComponentInChildren<ConversantFinder>();
            if (PlayerConversant == null) PlayerConversant = GetComponent<PlayerConversant>();
            if (ShopFinder == null) ShopFinder = GetComponentInChildren<ShopFinder>();
            if (Shopper == null) Shopper = GetComponent<Shopper>();
            if (Health == null) Health = GetComponent<Health>();
            if (BaseStats == null) BaseStats = GetComponent<BaseStats>();
            if (CooldownTokenManager == null) CooldownTokenManager = GetComponent<CooldownTokenManager>();
        }

        public GameObject GetTarget()
        {
            return Targeter.CurrentTarget!=null? Targeter.CurrentTarget.gameObject : null;
        }
    }
}