﻿using RPG.Shops;

namespace RPG.States.Player
{
    public class PlayerShoppingState : PlayerBaseState
    {
        private Shop target;
        
        public PlayerShoppingState(PlayerStateMachine stateMachine, Shop shop) : base(stateMachine)
        {
            target = shop;
        }

        public override void Enter()
        {
            stateMachine.Shopper.SetActiveShop(target);
            stateMachine.Shopper.activeShopChange += Shopper_HandleActiveShopChange;
        }

        public override void Tick(float deltaTime)
        {
            
        }

        public override void Exit()
        {
            stateMachine.Shopper.activeShopChange -= Shopper_HandleActiveShopChange;
        }

        private void Shopper_HandleActiveShopChange()
        {
            if (stateMachine.Shopper.GetActiveShop() == null)
            {
                stateMachine.SwitchState(new PlayerFreeLookState(stateMachine));
            }
        }
    }
}