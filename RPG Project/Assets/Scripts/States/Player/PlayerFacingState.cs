﻿using UnityEngine;

namespace RPG.States.Player
{
    public class PlayerFacingState : PlayerBaseState
    {
        private PlayerBaseState nextState;
        private Vector3 target;
        private float timer;
        private static readonly int IdleHash = Animator.StringToHash("Idle");

        public PlayerFacingState(PlayerStateMachine stateMachine, Vector3 target, PlayerBaseState nextState) : base(stateMachine)
        {
            this.nextState = nextState;
            this.target = target;
        }

        public override void Enter()
        {
            if(nextState==null)
            {
                stateMachine.SwitchState(new PlayerFreeLookState(stateMachine));
                return;
            }
            stateMachine.Animator.CrossFadeInFixedTime(IdleHash, AnimatorDampTime);
        }

        public override void Tick(float deltaTime)
        {
            if (Vector3.Angle(stateMachine.transform.forward, target - stateMachine.transform.position) < 5.0f)
            {
                stateMachine.SwitchState(nextState);
                return;
            }
            timer += deltaTime;
            if (timer >= .5f)
            {
                stateMachine.SwitchState(nextState);
                return;
            }
            FaceTarget(target, deltaTime);
            Move(deltaTime);
        }

        public override void Exit()
        {
            
        }
    }
}