using UnityEngine;

namespace RPG.States.Player
{
    public class PlayerFreeLookState : PlayerBaseState
    {
        public PlayerFreeLookState(PlayerStateMachine stateMachine) : base(stateMachine)
        {
        }
        
        private static readonly int FreeLookBlendTreeHash = Animator.StringToHash("FreeLookBlendTree");
        private static readonly int FreeLookSpeedHash = Animator.StringToHash("FreeLookSpeed");
        

        public override void Enter()
        {
            stateMachine.InputReader.JumpEvent += InputReader_HandleJumpEvent;
            stateMachine.InputReader.TargetEvent += InputReader_HandleTargetEvent;
            stateMachine.InputReader.PickupEvent += InputReader_HandlePickupEvent;
            stateMachine.InputReader.DialogueEvent += InputReader_HandleDialogueEvent;
            stateMachine.InputReader.ShopEvent += InputReader_HandleShopEvent;
            stateMachine.Animator.CrossFadeInFixedTime(FreeLookBlendTreeHash, stateMachine.CrossFadeDuration);
        }



        public override void Tick(float deltaTime)
        {
            Vector3 movement = CalculateMovement();
            Move(movement*stateMachine.FreeLookMovementSpeed, deltaTime);
            if (stateMachine.InputReader.IsAttacking)
            {
                HandleAttackButtonPressed();
                return;
            }
            if (stateMachine.InputReader.MovementValue == Vector2.zero)
            {
                stateMachine.Animator.SetFloat(FreeLookSpeedHash, 0, AnimatorDampTime, deltaTime);
                if(stateMachine.Animator.GetFloat(FreeLookSpeedHash)<.1f) stateMachine.Animator.SetFloat(FreeLookSpeedHash, 0f);
                return;
            }
            stateMachine.Animator.SetFloat(FreeLookSpeedHash, movement.magnitude, AnimatorDampTime, deltaTime);
            FaceMovementDirection(movement, deltaTime);
        }

        public override void Exit()
        {
            stateMachine.InputReader.JumpEvent -= InputReader_HandleJumpEvent;
            stateMachine.InputReader.TargetEvent -= InputReader_HandleTargetEvent;
            stateMachine.InputReader.PickupEvent -= InputReader_HandlePickupEvent;
            stateMachine.InputReader.DialogueEvent -= InputReader_HandleDialogueEvent;
        }

        private void InputReader_HandleDialogueEvent()
        {
            if (stateMachine.ConversantFinder.FindNearestTarget())
            {
                PlayerConversantState conversantState =
                    new PlayerConversantState(stateMachine, stateMachine.ConversantFinder.CurrentTarget);
                stateMachine.SwitchState(new PlayerFacingState(stateMachine, stateMachine.ConversantFinder.CurrentTarget.transform.position,conversantState));
            }
        }

        void HandleAttackButtonPressed()
        {
            if (stateMachine.Targeter.HasTargets)
            {
                stateMachine.SwitchState(new PlayerAttackingState(stateMachine, 0));
                return;
            }

            if (stateMachine.PickupFinder.HasTargets)
            {
                InputReader_HandlePickupEvent();
                if (stateMachine.PickupFinder.CurrentTarget) return;
            }

            if (stateMachine.ConversantFinder.HasTargets)
            {
                InputReader_HandleDialogueEvent();
                if (stateMachine.ConversantFinder.CurrentTarget) return;
            }
            stateMachine.SwitchState(new PlayerAttackingState(stateMachine, 0));
        }


        private void FaceMovementDirection(Vector3 forward, float deltaTime)
        {
            if (forward == Vector3.zero) return;
            Quaternion desiredRotation = Quaternion.LookRotation(forward, Vector3.up);
            stateMachine.transform.rotation =
                Quaternion.Slerp(stateMachine.transform.rotation, desiredRotation, stateMachine.FreeLookRotationSpeed *deltaTime);
        }
        
        private Vector3 CalculateMovement()
        {
            Vector3 forward = stateMachine.MainCameraTransform.forward;
            Vector3 right = stateMachine.MainCameraTransform.right;
            forward.y = 0;
            right.y = 0;
            forward.Normalize();
            right.Normalize();
            Vector3 movement = right * stateMachine.InputReader.MovementValue.x;
            movement += forward * stateMachine.InputReader.MovementValue.y;
            return Vector3.Min(movement, movement.normalized);
        }
        
        
        
        private void InputReader_HandleJumpEvent()
        {
            Debug.Log($"I get up, and nothing gets me down");
        }
        
        private void InputReader_HandleTargetEvent()
        {
            if(stateMachine.Targeter.SelectTarget())
            {
                stateMachine.SwitchState(new PlayerTargetingState(stateMachine));
            }
        }
        
        private void InputReader_HandlePickupEvent()
        {
            if (stateMachine.PickupFinder.FindNearestTarget())
            {
                stateMachine.SwitchState(new PlayerPickupState(stateMachine, stateMachine.PickupFinder.CurrentTarget));
            }
        }
        
        private void InputReader_HandleShopEvent()
        {
            if (stateMachine.ShopFinder.FindNearestTarget())
            {
                PlayerShoppingState nextState =
                    new PlayerShoppingState(stateMachine, stateMachine.ShopFinder.CurrentTarget);
                stateMachine.SwitchState(new PlayerFacingState(stateMachine, stateMachine.ShopFinder.CurrentTarget.transform.position, nextState));
            }
        }
    }
}