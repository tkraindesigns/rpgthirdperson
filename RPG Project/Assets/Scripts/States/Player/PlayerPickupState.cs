﻿using RPG.Inventories;
using UnityEngine;

namespace RPG.States.Player
{
    public class PlayerPickupState : PlayerBaseState
    {
        private static readonly int PickupHash = Animator.StringToHash("Pickup");

        public PlayerPickupState(PlayerStateMachine stateMachine, PickupTarget target) : base(stateMachine)
        {
            this.target = target;
            
        }

        private PickupTarget target;
        private Vector3 position;
        
        public override void Enter()
        {
            if (target == null)
            {
                stateMachine.SwitchState(new PlayerFreeLookState(stateMachine));
                return;
            }

            position = target.transform.position;
            stateMachine.Animator.CrossFadeInFixedTime(PickupHash, AnimatorDampTime);
            stateMachine.AnimationEventRelay.PickupItemEvent += AnimationEventRelay_HandlePickup;
        }

        public override void Tick(float deltaTime)
        {
            FaceTarget(position, deltaTime);
            Move(deltaTime);
            if (GetNormalizedTime("Pickup") > 0.80f)
            {
                stateMachine.SwitchState(new PlayerFreeLookState(stateMachine));
            }
        }

        public override void Exit()
        {
            stateMachine.AnimationEventRelay.PickupItemEvent -= AnimationEventRelay_HandlePickup;
        }

        void AnimationEventRelay_HandlePickup()
        {
            target.PickupItem();
        }
    }
}