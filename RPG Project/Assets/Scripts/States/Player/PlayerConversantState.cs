﻿using RPG.Dialogue;

namespace RPG.States.Player
{
    public class PlayerConversantState : PlayerBaseState
    {
        private AIConversant targetConversant;
        
        public PlayerConversantState(PlayerStateMachine stateMachine, AIConversant targetConversant) : base(stateMachine)
        {
            this.targetConversant = targetConversant;
        }

        public override void Enter()
        {
            if (!targetConversant)
            {
                stateMachine.SwitchState(new PlayerFreeLookState(stateMachine));
                return;
            }
            stateMachine.PlayerConversant.onConversationUpdated += PlayerConversant_OnConversationUpdated;
            stateMachine.PlayerConversant.StartDialogue(targetConversant, targetConversant.GetDialogue());
        }

        public override void Tick(float deltaTime)
        {
            
        }

        public override void Exit()
        {
            stateMachine.PlayerConversant.onConversationUpdated += PlayerConversant_OnConversationUpdated;
        }

        void PlayerConversant_OnConversationUpdated()
        {
            if (!stateMachine.PlayerConversant.IsActive())
            {
                stateMachine.SwitchState(new PlayerFreeLookState(stateMachine));
            }
        }
        
    }
}