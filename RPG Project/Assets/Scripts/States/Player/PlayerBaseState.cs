﻿using UnityEngine;

namespace RPG.States.Player
{
    public abstract class PlayerBaseState : State
    {
        protected PlayerStateMachine stateMachine;

        protected float AnimatorDampTime = 0.1f;
        
        public PlayerBaseState(PlayerStateMachine stateMachine)
        {
            this.stateMachine = stateMachine;
        }

        protected void Move(float deltaTime)
        {
            Move(Vector3.zero, deltaTime);
        }
        
        protected void Move(Vector3 direction, float deltaTime)
        {
            stateMachine.CharacterController.Move((direction + stateMachine.ForceReceiver.Movement) * deltaTime);
        }

        protected void FaceTarget(Vector3 target, float deltaTime)
        {
            Vector3 directionToTarget = target - stateMachine.transform.position;
            directionToTarget.y = 0;
            stateMachine.transform.rotation = Quaternion.Slerp(stateMachine.transform.rotation, Quaternion.LookRotation(directionToTarget), stateMachine.FreeLookRotationSpeed * deltaTime);
        }

        protected void SetLocomotionState()
        {
            stateMachine.SwitchState(stateMachine.Targeter.CurrentTarget?new PlayerTargetingState(stateMachine) : new PlayerFreeLookState(stateMachine));
        }

        protected float GetNormalizedTime(string tag = "Attack")
        {
            var currentInfo= stateMachine.Animator.GetCurrentAnimatorStateInfo(0);
            var nextInfo = stateMachine.Animator.GetNextAnimatorStateInfo(0);
            if (stateMachine.Animator.IsInTransition(0) && nextInfo.IsTag(tag))
            {
                return nextInfo.normalizedTime;
            }
            else if(!stateMachine.Animator.IsInTransition(0) && currentInfo.IsTag(tag))
            {
                return currentInfo.normalizedTime;
            }

            return 0;
        }
    }
}