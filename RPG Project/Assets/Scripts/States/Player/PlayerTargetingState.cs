﻿using UnityEngine;

namespace RPG.States.Player
{
    public class PlayerTargetingState : PlayerBaseState
    {
        
        private static readonly int TargetingBlendTreeHash = Animator.StringToHash("TargetingBlendTree");
        private static readonly int TargetingForwardSpeedHash = Animator.StringToHash("TargetingForwardSpeed");
        private static readonly int TargetingRightSpeedHash = Animator.StringToHash("TargetingRightSpeed");

        public PlayerTargetingState(PlayerStateMachine stateMachine) : base(stateMachine)
        {
        }

        public override void Enter()
        {
            stateMachine.InputReader.TargetEvent+=CancelTargeting;
            stateMachine.Animator.CrossFadeInFixedTime(TargetingBlendTreeHash, stateMachine.CrossFadeDuration);
        }



        public override void Tick(float deltaTime)
        {
            if (stateMachine.Targeter.CurrentTarget == null)
            {
                stateMachine.SwitchState(new PlayerFreeLookState(stateMachine));
                return;
            }
            if (stateMachine.InputReader.IsAttacking)
            {
                stateMachine.SwitchState(new PlayerAttackingState(stateMachine, 0));
                return;
            }
            FaceTarget(stateMachine.Targeter.CurrentTarget.transform.position, deltaTime);
            Move(CalculateMovement() * stateMachine.TargetingMovementSpeed, deltaTime);
            stateMachine.Animator.SetFloat(TargetingForwardSpeedHash, stateMachine.InputReader.MovementValue.y);
            stateMachine.Animator.SetFloat(TargetingRightSpeedHash, stateMachine.InputReader.MovementValue.x);
        }

        public override void Exit()
        {
            stateMachine.InputReader.TargetEvent -= CancelTargeting;
        }
        
        private void CancelTargeting()
        {
            stateMachine.Targeter.Cancel();
            stateMachine.SwitchState(new PlayerFreeLookState(stateMachine));
        }

        Vector3 CalculateMovement()
        {
            Vector3 movement = new Vector3();
            movement += stateMachine.transform.right * stateMachine.InputReader.MovementValue.x;
            movement += stateMachine.transform.forward * stateMachine.InputReader.MovementValue.y;
            return Vector3.Min(movement, movement.normalized);
        }
    }
}