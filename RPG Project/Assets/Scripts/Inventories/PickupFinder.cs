﻿using System.Linq;
using RPG.Core;
using UnityEngine;

namespace RPG.Inventories
{
    public class PickupFinder : RangeFinder<PickupTarget>
    {
        
        protected override void AddTarget(PickupTarget target)
        {
            base.AddTarget(target);
            target.OnPickedUp += RemoveTarget;
        }

        protected override void RemoveTarget(PickupTarget target)
        {
            base.RemoveTarget(target);
            target.OnPickedUp -= RemoveTarget;
        }
    }
}