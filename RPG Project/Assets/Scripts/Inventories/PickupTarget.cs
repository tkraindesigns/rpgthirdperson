﻿using GameDevTV.Inventories;
using RPG.Core;
using UnityEngine;

namespace RPG.Inventories
{
    public class PickupTarget : MonoBehaviour, ITarget
    {
        Pickup pickup;
        private Inventory inventory;

        public event System.Action<PickupTarget> OnPickedUp;

        private void Awake()
        {
            pickup = GetComponent<Pickup>();
            inventory = Inventory.GetPlayerInventory();
        }
        
        public bool IsValid()
        {
            return inventory.HasSpaceFor(pickup.GetItem());
        }

        private bool handled;
        public void PickupItem()
        {
            OnPickedUp?.Invoke(this);
            handled = true;
            pickup.PickupItem();
        }

        void OnDestroy()
        {
            if(!handled) OnPickedUp?.Invoke(this);
        }

    }
}