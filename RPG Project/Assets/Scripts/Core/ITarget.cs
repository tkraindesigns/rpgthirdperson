﻿namespace RPG.Core
{
    public interface ITarget
    {
        bool IsValid();
        
    }
}