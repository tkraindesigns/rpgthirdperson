﻿using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    public abstract class RangeFinder<T> : MonoBehaviour where T : MonoBehaviour, ITarget
    {
        protected List<T> Targets = new List<T>();
        
        public event System.Action<T> OnTargetAdded;
        public event System.Action<T> OnTargetRemoved;
        public T CurrentTarget { get; protected set; }

        public bool HasTargets => Targets.Count > 0;

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out T target))
            {
                if (target.IsValid())
                {
                    AddTarget(target);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.TryGetComponent(out T target))
            {
                RemoveTarget(target);
            }
        }

        
        protected virtual void AddTarget(T target)
        {
            Targets.Add(target);
            OnTargetAdded?.Invoke(target);
        }

        protected virtual void RemoveTarget(T target)
        {
            Targets.Remove(target);
            OnTargetRemoved?.Invoke(target);
        }

        public bool FindNearestTarget()
        {
            CurrentTarget = null;
            float closestDistance = Mathf.Infinity;
            foreach (T target in Targets)
            {
                float distance = Vector3.Distance(transform.position, target.transform.position);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    CurrentTarget = target;
                }
            }
            return CurrentTarget;
        }
    }
}