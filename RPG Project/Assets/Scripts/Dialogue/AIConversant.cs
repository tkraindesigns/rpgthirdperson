﻿using RPG.Attributes;
using RPG.Core;
using UnityEngine;

namespace RPG.Dialogue
{
    public class AIConversant : MonoBehaviour, ITarget
    {
        [SerializeField] Dialogue dialogue = null;
        [SerializeField] string conversantName;
        
        public string GetName()
        {
            return conversantName;
        }
        
        public Dialogue GetDialogue()
        {
            return dialogue;
        }

        public bool IsValid()
        {
            if (dialogue == null) return false;
            if (TryGetComponent(out Health health) && health.IsDead()) return false;
            return true;
        }
    }
}