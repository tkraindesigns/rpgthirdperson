﻿using RPG.Core;
using UnityEngine;

namespace RPG.Dialogue
{
    public class ConversantFinder : RangeFinder<AIConversant>
    {
        public AIConversant GetNearestConversant()
        {
            CurrentTarget = null;
            float closestDistance = Mathf.Infinity;
            foreach (AIConversant target in Targets)
            {
                float distance = Vector3.Distance(transform.position, target.transform.position);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    CurrentTarget = target;
                }
            }
            return CurrentTarget;
        }
    }
}