﻿using UnityEngine;

namespace RPG.Combat
{
    [CreateAssetMenu(fileName = "Attack", menuName = "Combat/New Attack", order = 0)]
    public class Attack : ScriptableObject
    {
        [field: SerializeField] public string AnimationName { get; private set; } = "Attack";
        [field: SerializeField] public bool ApplyRootMotion { get; private set; } = true;

        [field: Range(0.5f, 2.0f)] [field: Tooltip("This will be multiplied by the damage in the Current Weapon Config")]
        [field: SerializeField] public float DamageModifier { get; private set; } = 1.0f;

        [field: Range(0, 1)] [field: SerializeField] public float TransitionDuration { get; private set; } = .1f;

        [field: SerializeField] public Attack NextComboAttack { get; private set; }
        [field: Range(0, 0.99f)] [field: SerializeField] public float ComboAttackTime { get; private set; }= 0.99f;
        [field: Range(0,20)]
        [field:Tooltip("Amount of force applied to the attacker when ApplyAttackForce Animation Event is fired")]
        [field: SerializeField] public float AttackForce { get; private set; }= 5f;
        [field: Range(0,20)]
        [field: Tooltip("Amount of force applied to the target when a hit is registered.")]
        [field: SerializeField] public float HitForce { get; private set; } = 6f;
        [field: SerializeField] public float Cooldown { get; private set; } = 1f;
    }
}