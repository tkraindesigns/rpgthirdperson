﻿using UnityEngine;

namespace RPG.Combat
{
    public interface ITargetProvider
    {
        GameObject GetTarget();
    }
}