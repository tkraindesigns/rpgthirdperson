﻿using System;
using System.Linq;
using Cinemachine;
using RPG.Core;
using UnityEngine;

namespace RPG.Combat.Targeting
{
    public class Targeter : RangeFinder<Target>
    {
        [SerializeField] private CinemachineTargetGroup cinemachineTargetGroup;
        
        [Tooltip("The higher the value, the more precedence will be given to how close the target is to the targeter." +
                 "The lower the value, the more precedence will be given to how close the target is to the center of the screen.")]
        [Range(0,1)]
        [SerializeField] private float distanceWeight = .5f;

        private SphereCollider sphereCollider;
        

        private Camera mainCamera;

        private void Awake()
        {
            sphereCollider = GetComponent<SphereCollider>();
            if (transform.parent.gameObject.TryGetComponent(out Fighter fighter))
            {
                fighter.OnWeaponChanged += Handle_WeaponChanged;
            }
        }

        private void Start()
        {
            mainCamera= Camera.main;
            
        }

        private void Handle_WeaponChanged(WeaponConfig weaponConfig)
        {
            if (weaponConfig && sphereCollider)
            {
                sphereCollider.radius = weaponConfig.GetTargetingRange();
            }
        }


        protected override void AddTarget(Target target)
        {
            base.AddTarget(target);
            target.OnDeath += RemoveTarget;
        }


        
        protected override void RemoveTarget(Target target)
        {
            base.RemoveTarget(target);
            if(target==CurrentTarget) Cancel();
            target.OnDeath -= RemoveTarget;
        }

        public bool SelectTarget()
        {
            if (Targets.Count == 0) return false;
            Rect bounds = new Rect(0, 0, 1, 1);
            Target closestTarget = null;
            float closestDistanceToCenter = Mathf.Infinity;
            foreach (Target target in Targets.OrderByDescending(t=>Vector3.SqrMagnitude(t.transform.position-transform.position)))
            {
                Target candidate = null;
                Vector2 viewPos = mainCamera.WorldToViewportPoint(target.transform.position);
                if (!bounds.Contains(viewPos)) continue;
                Vector2 toCenter = viewPos - new Vector2(0.5f, 0.5f);
                float distanceToTarget = toCenter.sqrMagnitude;
                if (distanceToTarget < closestDistanceToCenter)
                {
                    candidate = target;
                } 
                else if (closestTarget != null)
                {
                    float distanceOfCandidateToCamera = Vector3.SqrMagnitude(target.transform.position - transform.position);
                    float distanceOfClosestToCamera =
                        Vector3.SqrMagnitude(closestTarget.transform.position - transform.position);
                    if (distanceOfCandidateToCamera > distanceOfClosestToCamera * distanceWeight)
                    {
                        continue;
                    }
                    candidate = target;
                }
                closestTarget = candidate;
                closestDistanceToCenter = distanceToTarget;
            }
            if (closestTarget == null) return false;
            CurrentTarget = closestTarget;
            cinemachineTargetGroup.AddMember(CurrentTarget.transform, 1, 2);
            return true;
        }

        public void Cancel()
        {
            if(CurrentTarget) cinemachineTargetGroup.RemoveMember(CurrentTarget.transform);
            CurrentTarget = null;
        }
    }


    
}