﻿using RPG.Attributes;
using RPG.Core;
using UnityEngine;

namespace RPG.Combat.Targeting
{
    [RequireComponent(typeof(Health))]
    public class Target : MonoBehaviour, ITarget
    {
        private Health health;

        private Health Health
        {
            get
            {
                if (!health) health = GetComponent<Health>();
                return health;
            }
        }
        /// <summary>
        /// Fulfills ITarget requirement.  For this class, IsValid means the character is alive.
        /// </summary>
        /// <returns>This character is alive and can be targeted.</returns>
        public bool IsValid() => !health.IsDead();

        private void Awake()
        {
            Health.onDie.AddListener(()=>
            {
                OnDeath?.Invoke(this);
            });
        }

        public event System.Action<Target> OnDeath;

    }
}