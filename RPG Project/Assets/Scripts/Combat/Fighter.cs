using UnityEngine;
using RPG.Movement;
using RPG.Core;
using RPG.Attributes;
using RPG.Stats;
using System.Collections.Generic;
using GameDevTV.Utils;
using GameDevTV.Inventories;

namespace RPG.Combat
{
    public class Fighter : MonoBehaviour, IAction
    {
        [SerializeField] float timeBetweenAttacks = 1f;
        [SerializeField] Transform rightHandTransform = null;
        [SerializeField] Transform leftHandTransform = null;
        [SerializeField] private Transform rightFootTransform = null;
        [SerializeField] private Transform leftFootTransform = null;
        [SerializeField] WeaponConfig defaultWeapon = null;
        [SerializeField] float autoAttackRange = 4f;

        public event System.Action<WeaponConfig> OnWeaponChanged;
        
        private Health target;
        
        private Equipment equipment;
        private ForceReceiver forceReceiver;
        private BaseStats baseStats;
        private bool isPlayer;
        
        float timeSinceLastAttack = Mathf.Infinity;
        WeaponConfig currentWeaponConfig;
        LazyValue<Weapon> currentWeapon;

        private Attack currentAttack;

        /// <summary>
        /// Get specified animation by index, and set the current Attack.
        /// </summary>
        /// <param name="attack"></param>
        /// <returns></returns>
        public Attack GetCurrentAttack(int attack)
        {
            if (currentWeaponConfig == null || currentWeaponConfig.Attacks.Length==0) return null;
            if (attack < 0 || attack >= currentWeaponConfig.Attacks.Length)
            {
                attack = 0;
            }
            currentAttack =  currentWeaponConfig.Attacks[attack];
            return currentAttack;
        }

        /// <summary>
        /// Sets the current attack.  Useful when attack contains a combo attack follow on.
        /// </summary>
        /// <param name="attack"></param>
        public void SetCurrentAttack(Attack attack)
        {
            currentAttack = attack;
        }
        
        private void Awake()
        {
            baseStats = GetComponent<BaseStats>();
            currentWeaponConfig = defaultWeapon;
            currentWeapon = new LazyValue<Weapon>(SetupDefaultWeapon);
            equipment = GetComponent<Equipment>();
            if (equipment)
            {
                equipment.equipmentUpdated += UpdateWeapon;
            }
            forceReceiver = GetComponent<ForceReceiver>();
            isPlayer = gameObject.CompareTag("Player");
        }

        private Weapon SetupDefaultWeapon()
        {
            return AttachWeapon(defaultWeapon);
        }

        private void Start() 
        {
            currentWeapon.ForceInit();
            OnWeaponChanged?.Invoke(currentWeaponConfig);
        }



        public void EquipWeapon(WeaponConfig weapon)
        {
            currentWeaponConfig = weapon;
            currentWeapon.value = AttachWeapon(weapon);
            OnWeaponChanged?.Invoke(currentWeaponConfig);
        }

        public int GetCurrentWeaponType()
        {
            if (currentWeaponConfig == null) return 0;
            return currentWeaponConfig.AnimatorWeaponType;
        }

        private void UpdateWeapon()
        {
            var weapon = equipment.GetItemInSlot(EquipLocation.Weapon) as WeaponConfig;
            if (weapon == null)
            {
                EquipWeapon(defaultWeapon);
            }
            else
            {
                EquipWeapon(weapon);
            }
        }

        private Weapon AttachWeapon(WeaponConfig weapon)
        {
            Animator animator = GetComponent<Animator>();
            return weapon.Spawn(rightHandTransform, leftHandTransform, animator);
        }

        public Health GetTarget()
        {
            return target;
        } 

        public Transform GetHandTransform(bool isRightHand)
        {
            if (isRightHand)
            {
                return rightHandTransform;
            }
            else
            {
                return leftHandTransform;
            }
        }
        




        // Animation Event
        void Hit()
        {
   
        }

        void Shoot()
        {
            if (!currentWeaponConfig.HasProjectile()) return;
            
            if (TryGetComponent(out ITargetProvider targetProvider))
            {
                float damage = GetComponent<BaseStats>().GetStat(Stat.Damage);
                GameObject targetObject = targetProvider.GetTarget();
                if (targetObject != null)
                {
                    currentWeaponConfig.LaunchProjectile(rightHandTransform, leftHandTransform, targetObject.GetComponent<Health>(), gameObject, damage);
                }
                else
                {
                    currentWeaponConfig.LaunchProjectile(rightHandTransform, leftHandTransform, gameObject, damage);
                }
            }
        }

        void TryHit(int slot)
        {
            if (currentAttack == null) return;
            Vector3 transformPoint;
            float damageRadius = .5f;
            switch (slot)
            {
                case 0: transformPoint = currentWeapon.value.DamagePoint;
                    damageRadius = currentWeapon.value.DamageRadius;
                    break;
                case 1: transformPoint = rightHandTransform.position;
                    break;
                case 2 : transformPoint = leftHandTransform.position;
                    break;
                case 3 : transformPoint = rightFootTransform.position;
                    break;
                case 4: transformPoint = leftFootTransform.position;
                    break;
                default: transformPoint = rightHandTransform.position;
                    break;
            }
            //Debug.Log($"Attacking with slot {slot}, position {transformPoint}");
            foreach (Collider other in Physics.OverlapSphere(transformPoint, damageRadius))
            {
                if (other.gameObject == gameObject) continue;
                if (other.TryGetComponent(out Health otherHealth) && !otherHealth.IsDead())
                {
                    Debug.Log($"Hitting {otherHealth.gameObject}");
                    float damage = baseStats.GetStat(Stat.Damage);
                    damage *= currentAttack.DamageModifier; //Allows for varying damage based on attack style.
                    if (other.TryGetComponent(out BaseStats otherBaseStats))
                    {
                        float defence = otherBaseStats.GetStat(Stat.Defence);
                        damage /= 1 + defence / damage;
                    }
                    otherHealth.TakeDamage(gameObject, damage);
                    TryApplyHitForce(other, transform.position);
                }
            }
        }

        private void TryApplyHitForce(Collider other, Vector3 transformPoint)
        {
            other.GetComponent<ForceReceiver>().AddForce((other.transform.position - transformPoint).normalized * currentAttack.HitForce, true);
        }


        void ApplyAttackForce()
        {
            if (!forceReceiver) return;
            forceReceiver.AddForce(transform.forward * currentAttack.AttackForce);
        }
        
        private bool GetIsInRange(Transform targetTransform)
        {
            return Vector3.Distance(transform.position, targetTransform.position) < currentWeaponConfig.GetRange();
        }

        public bool CanAttack(GameObject combatTarget)
        {
            if (combatTarget == null) { return false; }
            if (!GetComponent<Mover>().CanMoveTo(combatTarget.transform.position) &&
                !GetIsInRange(combatTarget.transform)) 
            {
                return false; 
            }
            Health targetToTest = combatTarget.GetComponent<Health>();
            return targetToTest != null && !targetToTest.IsDead();
        }

        public void Attack(GameObject combatTarget)
        {
            if (isPlayer) return;
            GetComponent<ActionScheduler>().StartAction(this);
            target = combatTarget.GetComponent<Health>();
        }

        public void Cancel()
        {
            
        }
        

        public float GetAttackingRange()
        {
            return currentWeaponConfig.GetRange();
        }
    }
}