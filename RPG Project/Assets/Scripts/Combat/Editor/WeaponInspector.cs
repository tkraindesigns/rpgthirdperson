﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace RPG.Combat.Editors
{
    [CustomEditor(typeof(Weapon))]
    public class WeaponInspector : Editor
    {
        private SerializedProperty damagePoint;
        private SerializedProperty damageRadius;
        private SerializedProperty onHit;

        private Weapon weapon;
        
        private void OnEnable()
        {
            damagePoint = serializedObject.FindProperty(nameof(damagePoint));
            damageRadius = serializedObject.FindProperty(nameof(damageRadius));
            onHit = serializedObject.FindProperty(nameof(onHit));
            weapon = (Weapon)target;
        }
        
        public override void OnInspectorGUI()
        {
            //EditorGUILayout.PropertyField(damagePoint);
            EditorGUILayout.PropertyField(damageRadius);
            EditorGUILayout.PropertyField(onHit);
            serializedObject.ApplyModifiedProperties();
        }

        private void OnSceneGUI()
        {
            EditorGUI.BeginChangeCheck();
            Vector3 worldPosition = weapon.DamagePoint;
            Vector3 newDamagePoint = Handles.PositionHandle(worldPosition, Quaternion.identity);
            if (EditorGUI.EndChangeCheck())
            {
                damagePoint.vector3Value = weapon.transform.InverseTransformPoint(newDamagePoint);
                serializedObject.ApplyModifiedProperties();
            }
        }
    }
}