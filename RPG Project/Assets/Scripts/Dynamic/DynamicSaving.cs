﻿using System.Collections.Generic;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace RPG.Dynamic
{
    public class DynamicSaving : MonoBehaviour, ISaveable, IJsonSaveable
    {
        private Dictionary<DynamicEntity, DynamicCharacterConfiguration> entities = new ();

        /// <summary>
        /// Registers an entity with the DynamicSaving component.  Will automatically subscribe to the
        /// DynamicEntity's OnDestroy if the Entity is destroyed.  
        /// </summary>
        /// <param name="entity">An instantiated entity within the scene</param>
        /// <param name="configuration">DynamicCharacterConfiguration assigned to thsi character</param>
        public void RegisterDynamicEntity(DynamicEntity entity, DynamicCharacterConfiguration configuration)
        {
            entities[entity] = configuration;
            entity.entityDestroyed += dynamicEntity =>
            {
                if (entities.ContainsKey(dynamicEntity))
                {
                    entities.Remove(dynamicEntity);
                }
            };
        }

        /// <summary>
        /// Spawns a Dynamic Entity using the supplied DynamicCharacterConfiguration and registers it.
        /// </summary>
        /// <param name="configuration">Configuration to use.</param>
        /// <param name="position"></param>
        /// <param name="eulerAngles"></param>
        /// <returns>null if entity cannot be spawned.</returns>
        public DynamicEntity CreateDynamicEntity(DynamicCharacterConfiguration configuration, Vector3 position, Vector3 eulerAngles)
        {
            if (configuration == null) return null;
            DynamicEntity entity = configuration.Spawn(transform);
            if (entity == null) return null;
            entity.transform.position = position;
            entity.transform.eulerAngles = eulerAngles;
            RegisterDynamicEntity(entity, configuration);
            return entity;
        }
        
        
        public object CaptureState()
        {
            Dictionary<string, object> state = new();
            foreach (KeyValuePair<DynamicEntity,DynamicCharacterConfiguration> pair in entities)
            {
                if(pair.Key==null) continue; //Should be impossible
                state[pair.Value.CharacterID] = pair.Key.CaptureState();
            }
            return state;
        }

        public void RestoreState(object state)
        {
            if (state is Dictionary<string, object> stateDict)
            {
                foreach (KeyValuePair<string,object> pair in stateDict)
                {
                    DynamicCharacterConfiguration config = DynamicCharacterConfiguration.GetFromID(pair.Key);
                    if (config == null) continue;
                    var character = CreateDynamicEntity(config, transform.position, transform.eulerAngles);
                    if (character)
                    {
                        character.RestoreState(pair.Key);
                    }
                }
            }
        }

        public JToken CaptureAsJToken()
        {
            JObject state = new();
            foreach (KeyValuePair<DynamicEntity,DynamicCharacterConfiguration> pair in entities)
            {
                if(pair.Key==null) continue; //Should be impossible
                state[pair.Value.CharacterID] = pair.Key.CaptureAsJToken();
            }
            return state;
        }

        public void RestoreFromJToken(JToken state)
        {
            if (state is JObject stateDict)
            {
                foreach (KeyValuePair<string,JToken> pair in stateDict)
                {
                    DynamicCharacterConfiguration config = DynamicCharacterConfiguration.GetFromID(pair.Key);
                    if (config == null) continue;
                    var character = CreateDynamicEntity(config, transform.position, transform.eulerAngles);
                    if (character)
                    {
                        character.RestoreFromJToken(pair.Key);
                    }
                }
            }
        }
    }
}