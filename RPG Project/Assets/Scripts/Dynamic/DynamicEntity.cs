using System;
using System.Collections.Generic;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace RPG.Dynamic
{
    /// <summary>
    /// Use this class instead of a SaveableEntity on any entity that is dynamically spawned into the scene.
    /// </summary>
    public class DynamicEntity : MonoBehaviour
    {
        public event System.Action<DynamicEntity> entityDestroyed;

        public object CaptureState()
        {
            Dictionary<string, object> state = new Dictionary<string, object>();
            foreach (ISaveable saveable in GetComponents<ISaveable>())
            {
                state[saveable.GetType().ToString()] = saveable.CaptureState();
            }
            return state;
        }
        
        public void RestoreState(object state)
        {
            Dictionary<string, object> stateDict = (Dictionary<string, object>)state;
            foreach (ISaveable saveable in GetComponents<ISaveable>())
            {
                string typeString = saveable.GetType().ToString();
                if (stateDict.ContainsKey(typeString))
                {
                    saveable.RestoreState(stateDict[typeString]);
                }
            }
        }

        public JToken CaptureAsJToken()
        {
            JObject state = new JObject();
            foreach (IJsonSaveable saveable in GetComponents<IJsonSaveable>())
            {
                state[saveable.GetType().ToString()] = saveable.CaptureAsJToken();
            }
            return state;
        }

        public void RestoreFromJToken(JToken state)
        {
            if (state is JObject stateDict)
            {
                foreach (IJsonSaveable saveable in GetComponents<IJsonSaveable>())
                {
                    string typeString = saveable.GetType().ToString();
                    if (stateDict.ContainsKey(typeString))
                    {
                        saveable.RestoreFromJToken(stateDict[typeString]);
                    }
                }
            }
        }
        
        private void OnDestroy()
        {
            entityDestroyed?.Invoke(this);
        }
    }
}