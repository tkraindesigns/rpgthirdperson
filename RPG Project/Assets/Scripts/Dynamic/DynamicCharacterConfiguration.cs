﻿using System.Collections.Generic;
using UnityEngine;

namespace RPG.Dynamic
{
    [CreateAssetMenu(fileName = "Dynamic Character Config", menuName = "Dynamic/Character Configuration", order = 0)]
    public class DynamicCharacterConfiguration : ScriptableObject
    {
        [SerializeField] private string characterID = "";
        [SerializeField] private GameObject characterPrefab;

        /// <summary>
        /// Spawns in the characterPrefab with the supplied transform as the owner.  The calling object
        /// is responsible for placing the character and restoring it's state if applicable.
        /// </summary>
        /// <param name="owner">Owner of the resulting entity, generally the calling class.</param>
        /// <returns>null if no prefab or prefab does not contain a DynamicEntity</returns>
        public DynamicEntity Spawn(Transform owner)
        {
            if (characterPrefab == null) return null;
            GameObject go = Instantiate(characterPrefab, owner);
            DynamicEntity entity = go.GetComponent<DynamicEntity>();
            return entity;
        }


        public string CharacterID => characterID;
        
        
        private void OnValidate()
        {
            // prevent prefabs from being assigned that do not have a DynamicEntity on them.
            if (characterPrefab != null && characterPrefab.TryGetComponent(out DynamicEntity dynamicEntity))
            {
                characterPrefab = null;
            }
            //Ensures there is always a characterID
            if (string.IsNullOrEmpty(characterID))
            {
                characterID = System.Guid.NewGuid().ToString();
                lookUp = null;
            }
        }

        private static Dictionary<string, DynamicCharacterConfiguration> lookUp;
        /// <summary>
        /// Gets the configuration from Resources with the given CharacterID. In order to be retrieved,
        /// the DynamicConfiguration must be in a folder named Resources (or a subfolder in a Resources folder).
        /// </summary>
        /// <param name="id">CharacterID to be retrieved</param>
        /// <returns>null if string is empty or id is not found</returns>
        public static DynamicCharacterConfiguration GetFromID(string id)
        {
            if (string.IsNullOrEmpty(id)) return null;
            if (lookUp == null)
            {
                lookUp = new Dictionary<string, DynamicCharacterConfiguration>();
                foreach (var configuration in Resources.LoadAll<DynamicCharacterConfiguration>(""))
                {
                    if (lookUp.ContainsKey(configuration.characterID))
                    {
                        Debug.LogWarning($"{configuration} and {lookUp[configuration.characterID]} have the same characterID");
                    }
                    else
                    {
                        lookUp[configuration.characterID] = configuration;
                    }
                }
            }
            if (lookUp.ContainsKey(id)) return lookUp[id];
            return null;
        }
    }
}