using UnityEngine;
using UnityEngine.AI;

namespace RPG.Movement
{
    public class ForceReceiver : MonoBehaviour
    {
        [SerializeField] private float drag = 0.3f;
        [SerializeField] private float minimumImpactVelocity = .1f;

        
        private CharacterController controller;
        private NavMeshAgent agent;

        public event System.Action<Vector3> OnForceApplied;
        public event System.Action OnForceCompleted;
    
        private void Awake()
        {
            controller = GetComponent<CharacterController>();
        }

        private float verticalVelocity;

        private Vector3 impact;
        private Vector3 dampingVelocity;
        private bool forceActive;

        public Vector3 Movement => impact + Vector3.up * verticalVelocity;

        
        
        private void Update()
        {
            if (verticalVelocity < 0f && controller.isGrounded)
            {
                verticalVelocity = Physics.gravity.y * Time.deltaTime;
            }
            else
            {
                verticalVelocity += Physics.gravity.y * Time.deltaTime;
            }
            impact = Vector3.SmoothDamp(impact, Vector3.zero, ref dampingVelocity, drag);
            if (forceActive)
            {
                if (impact.sqrMagnitude < minimumImpactVelocity)
                {
                    impact = Vector3.zero;
                    forceActive = false;
                    OnForceCompleted?.Invoke();
                    if (agent) agent.enabled = true;
                }
            }
        }

        public void AddForce(Vector3 force, bool triggerKnockbackEvent = false)
        {
            impact += force;
            if (agent) agent.enabled = false;
            if(triggerKnockbackEvent)
            {
                OnForceApplied?.Invoke(force);
            }
            forceActive = true;
        }
    }
}