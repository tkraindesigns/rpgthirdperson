﻿using UnityEngine;
using UnityEngine.Pool;

namespace Pooling
{
    public class StaticPool<T> where T : MonoBehaviour, IPoolable
    {
        private static ObjectPool<T> pool;
        

        public static void InitializePool(T prefab, int defaultCapacity = 10, int maxSize = 10000)
        {
            if (pool != null) return;
            if (!prefab) return;
            
            pool = new ObjectPool<T>(() =>
            {
                T t =  GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity);
                t.SetDisposeCallback(() => pool.Release(t));
                return t;
            }, poolable => poolable.gameObject.SetActive(true),
                poolable => poolable.gameObject.SetActive(false),
                poolable => GameObject.Destroy(poolable),
                false,
                defaultCapacity,
                maxSize);
        }
    }

    public interface IPoolable
    {
        void SetDisposeCallback(System.Action callback);
    }
}