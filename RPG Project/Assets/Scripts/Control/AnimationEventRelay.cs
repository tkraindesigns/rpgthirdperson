﻿using UnityEngine;

namespace RPG.Control
{
    public class AnimationEventRelay : MonoBehaviour
    {
        public event System.Action PickupItemEvent;
        
        void PickupItem()
        {
            PickupItemEvent?.Invoke();
        }
    }
}