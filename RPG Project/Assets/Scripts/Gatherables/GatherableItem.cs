﻿using UnityEngine;

namespace Gatherables
{
    [CreateAssetMenu(fileName = "New Gatherable Item", menuName = "RPG/Inventory/Gatherable Item", order = 0)]
    public class GatherableItem : ScriptableObject
    {
        [SerializeField] private GameObject modelPrefab;

        public GameObject Spawn(Transform parent, Vector3 position)
        {
            if (!modelPrefab)
            {
                Debug.Log($"{name} has no modelPrefab assigned!");
            }
            var model = Instantiate(modelPrefab, parent);
            model.transform.position = position;
            return model;
        }
    }
}